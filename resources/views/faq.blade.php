@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center ">FAQ</div>
                <div class="panel-body">
                  <h3>How can I invest with {{ config('app.name', '  ') }}  ?</h3> <br>
To make a investment you must first become a member of {{ config('app.name', '  ') }} . Once you are signed up, you can make your first deposit. All deposits must be made through the Members Area. You can login using the member username and password you receive when signup. 

<h3>How do I open my {{ config('app.name', '  ') }}  Account?</h3> <br>
It's quite easy and convenient. Follow this link, fill in the registration form and then press "Register". 

<h3>How can I withdraw funds?</h3> <br>
Login to your account using your username and password and check the Withdraw section. 

<h3>How long does it take for my deposit to be added to my account?</h3> <br>
Your account will be updated as fast, as the networking payment process it. 

<h3>What if I can't log into my account because I forgot my password?</h3> <br>
Click forgot password link, type your username or e-mail and you'll receive your account information. 

<h3>Can I do a direct deposit from my account balance?</h3> <br>
Yes! To make a deposit from your {{ config('app.name', '  ') }}   account balance. Simply login into your members account and click on Make Deposit ans select the Deposit from Account Balance Radio button. 

<h3>Can I make an additional deposit to {{ config('app.name', '  ') }}  account once it has been opened?</h3> <br>
Yes, you can but all transactions are handled separately. 

<h3>How can I change my password?</h3> <br>
You can change your password directly from your members area by editing it in your personal profile. 

<h3>Can I lose money?</h3> <br>
There is a risk involved with investing in all high yield investment programs. However, there are a few simple ways that can help you to reduce the risk of losing more than you can afford to. First, align your investments with your financial goals, in other words, keep the money you may need for the short-term out of more aggressive investments, reserving those investment funds for the money you intend to raise over the long-term. It's very important for you to know that we are real traders and that we invest members' funds on major investments. 

<h3>How can I check my account balances?</h3> <br>
You can access the account information 24 hours, seven days a week over the Internet. 

<h3>May I open several accounts in your program?</h3> <br>
No. If we find that one member has more that one account, the entire funds will be frozen. 

<h3>How can I make a spend?</h3> <br>
To make a spend you must first become a member of {{ config('app.name', '  ') }} . Once you are signed up, you can make your first spend. All spends must be made through the Member Area. You can login using the member username and password you received when signup. 

<h3>Who manages the funds?</h3> <br>
These funds are managed by a team of {{ config('app.name', '  ') }} rofessional investment experts.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
