@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Investment</div>
                <div class="panel-body">
                @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                <div class="alert alert-danger">
        <ul>
                            <li>You dont have funds to withdrawal </li>
                    </ul>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
