<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DailyBitsPro Soon</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap and Font Awesome css-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <!-- Google fonts-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Pacifico">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,700">
    <!-- Theme stylesheet-->
    <link rel="stylesheet" href="http://www.dailybitspro.com/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <!-- <link rel="shortcut icon" href="favicon.png"> -->
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div style="background-image: url('http://www.dailybitspro.com/img/beach.jpg')" class="main"> 
      <div class="overlay"></div>
      <div class="container">
        <p class="social"><a href="https://www.facebook.com/groups/1323756967644411/" title="" class="facebook"><i class="fa fa-facebook"></i></a><a href="#" title="" class="twitter"><i class="fa fa-twitter"></i></a><a href="#" title="" class="gplus"><i class="fa fa-google-plus"></i></a><a href="#" title="" class="instagram"><i class="fa fa-instagram"></i></a></p>
        <div class="row">
          <div class="col-sm-12">
            <h1 class="cursive">DailyBitspro</h1>
            <h2 class="sub">We are still tweaking few details, stay tuned!</h2>
          </div>
        </div>


<div class="col-xs-12 col-sm-12">
          <div class="panel panel-default background-black text-white">
            <div class="panel-heading text-center ">Register</div>
            <div class="panel-body">
              <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">{{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">Name</label>
                  <div class="col-md-6">
                  <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                  @if ($errors->has('name'))<span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                            @endif
                  </div>
                </div>
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                  <label for="username" class="col-md-4 control-label">Username</label>
                  <div class="col-md-6">
                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                    @if ($errors->has('username'))<span class="help-block"><strong>{{ $errors->first('username') }}</strong></span>@endif
                  </div>
                </div>

                              



                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"><label for="email" class="col-md-4 control-label">E-Mail Address</label>
                  <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
                  </div>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Password</label>
                    <div class="col-md-6">
                      <input id="password" type="password" class="form-control" name="password" required>
                      @if ($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
                    </div>
                </div>
                <div class="form-group">
                  <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                  <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                  </div>
                </div>
                <div style="display:none;" class="form-group{{ $errors->has('sponsor') ? ' has-error' : '' }}">
                  <label for="sponsor" class="col-md-4 control-label">Sponsor</label>
                  <div class="col-md-6">
                    <input id="sponsor" type="sponsor" class="form-control" name="sponsor" value="<?= (Route::current()->getParameter('sponsor')!="") ? Route::current()->getParameter('sponsor') : "" ?>">
                    @if ($errors->has('sponsor'))<span class="help-block"><strong>{{ $errors->first('sponsor') }}</strong></span>@endif
                  </div>
                </div>

                  <div class="form-group{{ $errors->has('perfectmoney') ? ' has-error' : '' }}">
                  <label for="perfectmoney" class="col-md-4 control-label">Perfect Money ID</label>
                  <div class="col-md-6">
                    <input id="perfectmoney" type="text" class="form-control" name="perfectmoney" value="{{ old('perfectmoney') }}" required autofocus>
                    @if ($errors->has('perfectmoney'))<span class="help-block"><strong>{{ $errors->first('perfectmoney') }}</strong></span>@endif
                  </div>
                </div>
                                <div class="form-group{{ $errors->has('btcwallet') ? ' has-error' : '' }}">
                  <label for="btcwallet" class="col-md-4 control-label">Wallet BTC</label>
                  <div class="col-md-6">
                    <input id="btcwallet" type="text" class="form-control" name="btcwallet" value="{{ old('btcwallet') }}" required autofocus>
                    @if ($errors->has('btcwallet'))<span class="help-block"><strong>{{ $errors->first('btcwallet') }}</strong></span>@endif
                  </div>
                </div>
                <div class="form-group"><div class="col-md-6 col-md-offset-4 "><button type="submit" class="btn btn-primary pull-right">Register</button></div></div>
              </form>
            </div>
          </div>

        </div>





        <!-- countdown-->
        <div id="countdown" class="countdown">
          <div class="row">
            <div class="countdown-item col-sm-3 col-xs-6">
              <div id="countdown-days" class="countdown-number">&nbsp;</div>
              <div class="countdown-label">days</div>
            </div>
            <div class="countdown-item col-sm-3 col-xs-6">
              <div id="countdown-hours" class="countdown-number">&nbsp;</div>
              <div class="countdown-label">hours</div>
            </div>
            <div class="countdown-item col-sm-3 col-xs-6">
              <div id="countdown-minutes" class="countdown-number">&nbsp;</div>
              <div class="countdown-label">minutes</div>
            </div>
            <div class="countdown-item col-sm-3 col-xs-6">
              <div id="countdown-seconds" class="countdown-number">&nbsp;</div>
              <div class="countdown-label">seconds</div>
            </div>
          </div>
        </div>









 








      <div class="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy;2017 DailyBitsPro</p>
            </div>
            <div class="col-md-6">
              <p class="credit">Made by <a href="#">OwHyip</a></p>
              <!-- Please do not remove the backlink to us unless you support the development at https://bootstrapious.com/donate. It is part of the license conditions. Thanks for understanding :) -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- JAVASCRIPT FILES -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="javascripts/vendor/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="http://www.dailybitspro.com/js/jquery.cookie.js"></script>
    <script src="http://www.dailybitspro.com/js/jquery.countdown.min.js"></script>
    <script src="http://www.dailybitspro.com/js/front.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
    
  </body>
</html>