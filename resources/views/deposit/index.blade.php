@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    <link href="/css/deposit.css" rel="stylesheet">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                {!! Form::open([ 'route  ' => 'DepositMake' ]) !!}
                        {!! Form::token() !!}
                        <div class="col-md-6 " >
                            <label>
                              <input type="radio" name="method" value="pm" style="display: none; " />
                              <img class="img-responsive" style="height: 15vh; min-width:     100%; " src="http://cashpk.com/wp-content/uploads/2010/09/perfect_money_forex_logo_png.png" >
                            </label>
                        </div>        
                        <div class="col-md-6" >
                            <label>   
                                    <input type="radio" name="method" value="bit" style=" display: none;" />   
                                    <img class="img-responsive" style="height: 15vh; min-width:     100%; " src="http://ukash-wallet.com/templates/Pisces/img/bitcoin-to-paypal-skrill-perfectmoney-webmoney-exchange.png">
                            </label>
                        </div>
                        <div class="form-group">
                            {!! Form::number('deposit', null, ['step'=>'0.01','class' => 'form-control', 'placeholder'=>'Enter your amount']) !!}
                        </div>              
                        <div class="form-group">
                            {!! Form::submit('Deposit', ['class' => 'btn btn-primary']) !!}
                        </div>              
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
