@extends('layouts.app')
@section('content')

<!-- MAIN BANNER -->
<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 col-xs-12">
        <img class="img-responsive banner" src="{{ asset('/img/screen.jpg') }}"">
      </div>
    </div>
  </div>
</section>

<!-- HOW ITS WORKS STEPS -->
<section class="background-green">
  <div class="container text-center text-white">
    <div class="row">   
      <div class="col-md-4 col-xs-12"><hr class="hr-title"></div>
      <div class="col-md-4 col-xs-12 sectiontittle"><h2 class="title text-center capitalize">how it works</h2></div>
      <div class="col-md-4 col-xs-12"><hr class="hr-title"></div>
    </div>
    <div class="row">
      <div class="col-sm-3 col-xs-6">
        <h2 class="step mayus bolder em2"> create an account </h2>
        <div class="btn-circle btn-xl background-yellow"><h2 class="step-number text-white "> 1</h2></div>
      </div>
      <div class="col-sm-3 col-xs-6">
        <h2 class="step mayus bolder em2"> deposit your funds </h2>
        <div class="btn-circle btn-xl background-yellow"><h2 class="step-number text-white "> 2</h2></div>
      </div>
      <div class="col-sm-3 col-xs-6">
          <h2 class="step mayus bolder em2"> inverst your funds 
          </h2>
          <div class="btn-circle btn-xl background-yellow"><h2 class="step-number text-white "> 3</h2></div>
      </div>
      <div class="col-sm-3 col-xs-6">
          <h2 class="step mayus bolder em2"> Withdrawal earnigns </h2>
          <div class="btn-circle btn-xl background-yellow"><h2 class="step-number text-white "> 4</h2></div>
      </div>
    </div>
  </div>  
</section>

<!-- FEATURES -->
<section class="background-blue">
  <div class="container">
    <div class="row">   
    <div class="col-md-4 col-xs-12"><hr class="hr-title"></div>
    <div class="col-md-4 col-xs-12 sectiontittle"><h2 class="title text-center text-white bolder">Features</h2></div>
    <div class="col-md-4 col-xs-12"><hr class="hr-title"></div>
    </div>
    <div class="row text-white"> 
      <div class="col-xs-12 col-md-6 em2">
        <ul> 
          <li>Earn 3% on laboral days</li>
          <li>Minimal investment: 7$</li>
          <li>Withdrawal from 3$</li>
          <li>Manual Payment</li>
          <li>One pay request per day</li>
          <li>6% for sales agent</li>
        </ul>
      </div>
      <div class="col-xs-12 col-md-6 em2">
        <ul> 
          <li>3% for referrals</li>
          <li>No repurchase rules</li>
          <li>Reflected earnins each hour</li>
          <li>1$ for registration</li>
          <li>Payment in 0-72 hours</li>
        </ul>
      </div>
    </div>
  </div>
</section>
    

<!-- WE STARTED -->
<section class="background-green">
<div class="container">
  <!-- <div class="container background-green bolder"> -->
    <div class="row">   
      <div class="col-md-4 col-xs-12"><hr class="hr-title"></div>
      <div class="col-md-4 col-xs-12 sectiontittle"><h2 class="title text-center text-white bolder">We Started</h2></div>
      <div class="col-md-4 col-xs-12"><hr class="hr-title"></div>
      </div>
    <div class="row">   
      @if (Auth::guest())
        <div class="col-xs-12 col-sm-6">
          <div class="panel panel-default background-black text-white">
            <div class="panel-heading text-center ">Register</div>
            <div class="panel-body">
              <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">{{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">Name</label>
                  <div class="col-md-6">
                  <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                  @if ($errors->has('name'))<span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                            @endif
                  </div>
                </div>
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                  <label for="username" class="col-md-4 control-label">Username</label>
                  <div class="col-md-6">
                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                    @if ($errors->has('username'))<span class="help-block"><strong>{{ $errors->first('username') }}</strong></span>@endif
                  </div>
                </div>

                              



                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"><label for="email" class="col-md-4 control-label">E-Mail Address</label>
                  <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
                  </div>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Password</label>
                    <div class="col-md-6">
                      <input id="password" type="password" class="form-control" name="password" required>
                      @if ($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
                    </div>
                </div>
                <div class="form-group">
                  <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                  <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                  </div>
                </div>
                <div style="display:none;" class="form-group{{ $errors->has('sponsor') ? ' has-error' : '' }}">
                  <label for="sponsor" class="col-md-4 control-label">Sponsor</label>
                  <div class="col-md-6">
                    <input id="sponsor" type="sponsor" class="form-control" name="sponsor" value="<?= (Route::current()->getParameter('sponsor')!="") ? Route::current()->getParameter('sponsor') : "" ?>">
                    @if ($errors->has('sponsor'))<span class="help-block"><strong>{{ $errors->first('sponsor') }}</strong></span>@endif
                  </div>
                </div>

                  <div class="form-group{{ $errors->has('perfectmoney') ? ' has-error' : '' }}">
                  <label for="perfectmoney" class="col-md-4 control-label">Perfect Money ID</label>
                  <div class="col-md-6">
                    <input id="perfectmoney" type="text" class="form-control" name="perfectmoney" value="{{ old('perfectmoney') }}" required autofocus>
                    @if ($errors->has('perfectmoney'))<span class="help-block"><strong>{{ $errors->first('perfectmoney') }}</strong></span>@endif
                  </div>
                </div>
                                <div class="form-group{{ $errors->has('btcwallet') ? ' has-error' : '' }}">
                  <label for="btcwallet" class="col-md-4 control-label">Wallet BTC</label>
                  <div class="col-md-6">
                    <input id="btcwallet" type="text" class="form-control" name="btcwallet" value="{{ old('btcwallet') }}" required autofocus>
                    @if ($errors->has('btcwallet'))<span class="help-block"><strong>{{ $errors->first('btcwallet') }}</strong></span>@endif
                  </div>
                </div>
                <div class="form-group"><div class="col-md-6 col-md-offset-4 "><button type="submit" class="btn btn-primary pull-right">Register</button></div></div>
              </form>
            </div>
          </div>

        </div>
         <div class="col-xs-12 col-sm-6">
          <h1 class="title text-center text-white">Payment Systems</h1>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                  <img class="img-responsive banner img-center" src="https://upload.wikimedia.org/wikipedia/en/f/ff/Perfect_Money.png"">
              </div>
              <div class="col-sm-12 col-xs-12">
                <img class="img-responsive banner img-center" src="{{ asset('/img/bitcoin.png') }}"">
              </div>
            </div>
        </div>  {{-- container payment  --}}
               @else
              <div class="col-xs-6">  <h1 class="title capitalize text-center text-white">deposit</h1>  
              





  
  <link href="/css/deposit.css" rel="stylesheet">
      <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">

              <form class="form-horizontal" role="form" method="POST" action="{{ url('deposit') }}">{{ csrf_field() }}
        
            <div class="col-md-6" >
              <label>
                <input type="radio" name="method" value="pm" style="display: none; " />
                <img class="img-responsive" style="height: 15vh; min-width:  100%; " src="http://cashpk.com/wp-content/uploads/2010/09/perfect_money_forex_logo_png.png" >
              </label>
            </div>    
            <div class="col-md-6" >
              <label>   
                  <input type="radio" name="method" value="bit" style=" display: none;" />   
                  <img class="img-responsive" style="height: 15vh; min-width:  100%; " src="http://ukash-wallet.com/templates/Pisces/img/bitcoin-to-paypal-skrill-perfectmoney-webmoney-exchange.png">
              </label>
            </div>
            <div class="form-group">
              {!! Form::number('deposit', null, ['step'=>'0.01','class' => 'form-control', 'placeholder'=>'Enter your amount']) !!}
            </div>        
            <div class="form-group">
              {!! Form::submit('Deposit', ['class' => 'btn btn-primary']) !!}
            </div>        
          {{-- {!! Form::close() !!} --}}
          </form>
        </div>
      </div>





              </div>
              <div class="col-xs-6">
                <h1 class="title capitalize text-center text-white">Referral Link</h1>  
 <h3> <a class="referral" href="/sponsor/{{Auth::user()->username}}">http://www.dailybitspro.com/sponsor/{{Auth::user()->username}}</a></h3>
                <h1 class="title capitalize text-center text-white">inverst</h1>  
              <form class="form-horizontal" role="form" method="POST" action="{{ url('investment') }}">
              {{ csrf_field() }}
            <div class="form-group">
              {!! Form::number('inverst', null, ['step'=>'0.01','class' => 'form-control', 'placeholder'=>'Enter your amount']) !!}
            </div>        
            {!! Form::submit('Inverst', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}



<h1 class="title capitalize text-center text-white">withdrawal</h1>  
                  {!! Form::open(['route  ' => 'InverstMake']) !!}
               {!! Form::token() !!}
            <div class="form-group">
              {!! Form::number('inverst', null, ['step'=>'0.01','class' => 'form-control', 'placeholder'=>'Enter your amount']) !!}
            </div>        
            {!! Form::submit('Withdrawal', ['class' => 'btn btn-primary', 'disabled']) !!}
            {!! Form::close() !!}

              </div>
             
               @endif
    </div>
             
            <!-- </div>  {{-- container-fluid  --}} -->
              
    </div>  {{-- we start  --}}
</section>



@endsection
