<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name', '  ') }}</title>




    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">
        {{-- Styles  --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/css/AdminLTE.min.css">
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/css/skins/_all-skins.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @yield('css')
    <link href="/css/app.css" rel="stylesheet">
    <!-- CUSTOM CSS -->
    {{-- <link rel="stylesheet" href="custom.css"> --}}
    <link href="{{ asset('/css/layout.css') }}" rel="stylesheet">




</head>
<body>
    <div id="app">
        <nav id="menu-nav" class="navbar navbar-default background-yellow  topbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse navbar-fixed navbar-ex1-collapse">
                <ul class="nav navbar-nav text-green bolder mayus arial principal-menu">
                  <li><a href="/">Home</a></li>
                  <li><a href="/rules">Privacy</a></li>
                  <li><a href="/register" @if (!Auth::guest()) style="display:none;" @endif>Sign Up</a></li>
                  <li><a href="/faq">FAQ</a></li>
                  <li><a href="/">Contact us</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>

            <!-- SECOND NAVBAR; With Login -->
                 <div class="block"></div>
                 <nav id="main-nav" class="navbar navbar-default background-black topbar" role="navigation">
                    <div class="container ">

                            <div class="navbar-header">
                                <a class="" href="{{ url('/') }}">
                                   <img class="img-responsive main-logo" src="{{ asset('/img/logo1.png') }}">
                                </a>
                            </div>
                                <div class="dropdown main-menu-dropdown pull-left">
                                  <button class="btn btn-default dropdown-toggle text-uppercase main-menu-dropdown_button" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Menu
                                    <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                      <li><a href="/">Home</a></li>
                                      <li><a href="/">Privacy</a></li>
                                      <li><a href="/register" @if (!Auth::guest()) style="display:none;" @endif>Sign Up</a></li>
                                      <li><a href="/">FAQ</a></li>
                                      <li><a href="/">Contact us</a></li>
                                  </ul>
                                </div>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                                <!-- <ul class="nav navbar-nav"></ul> -->
                                <ul class="nav navbar-nav navbar-right text-white">
                                    <!-- Authentication Links -->
                                    @if (Auth::guest())
                                        <form id="signin" class="navbar-form navbar-right" role="form" method="POST" _lpchecked="1" action="{{ url('/login') }}">
                                            {{ csrf_field() }}
                                            <div class="input-group principal-login-input">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user text-white"></i></span>
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address" autocomplete="off">                                      
                                            </div>    
                                            <div class="input-group principal-login-input">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock text-white"></i></span>
                                                <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                         @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif            
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-grey mayus text-black bolder arial without-border principal-login-btn">Login</button>
                                        </form> 
                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                                        </span>
                                        @endif
                                    @else
                                    <li><a><b>My Balace</b> {{ Auth::user()->balance }} $</a></li>
                                    <li><a><b>My Invest</b> {{ App\Investment::where('user_id',Auth::user()->id)->where('status',1)->sum('investment') }} Usd</a></li>
                                    <li><a href="{{ url('/deposit') }}">Deposit</a></li>
                                    <li><a href="{{ url('/investment') }}">Investment</a></li>
                                    <li><a href="{{ url('/withdrawal') }}">Withdrawals</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{ url('/logout') }}"
                                                        onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                    </div>
                <!-- </div> /.navbar-collapse -->
                </nav>


@if (!Auth::guest())
         <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="#" class="logo">
                <b>InfyOm</b>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg"
                                     class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">{!! Auth::user()->name !!}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg"
                                         class="img-circle" alt="User Image"/>
                                    <p>
                                        {!! Auth::user()->name !!}
                                        <small>Member since {!! Auth::user()->created_at->format('M. Y') !!}</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <strong>Copyright © 2016 <a href="#">Company</a>.</strong> All rights reserved.
        </footer>

    </div>

@else

        @yield('content')


    @endif



<!-- FOOTER -->
        <section class="background-footer">
            <div class="container-fluid background-footer bolder">
                <div class="container background-footer bolder">
                    <div class="row">   
                    <div class="col-xs-4"> 
                      <a class="" href="{{ url('/') }}"><img class="img-responsive img-center" src="{{ asset('/img/logofooter239x41.png') }}"></a>
                    </div>
                    <div class="col-xs-8"> 
                      <a class="" href="{{ url('/') }}"><img class="img-responsive img-center" src="{{ asset('/img/logoservers671x54.png') }}"></a>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        
    <!-- credits -->
    <section class="background-green">
        <div class="container-fluid">
            <div class="container background-green bolder text-center">
                <div class="row text-center">   
                    <a class="" href="{{ url('/') }}"><img class="img-responsive img-center" src="{{ asset('/img/GIF.gif') }}"></a> 
                </div>
                <div class="row text-black">  
                    <h3>  Powered by <a href="#" class="owhyip text-white bolder">OwHyip</a> </h3>
                </div>
            </div>
        </div>
    </section>
    </div> {{-- app --}}
    <!-- Scripts -->

    <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4VZ1XXwkzgqr5gmv6GWLZ7NUTyb1hBqQ";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>

<!-- jQuery 2.1.4 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

    <!-- AdminLTE App -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.3/js/app.min.js"></script>


    <script src="{{ asset('/js/app.js') }}"></script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> --}}
    @yield('scripts')
</body>
</html>
