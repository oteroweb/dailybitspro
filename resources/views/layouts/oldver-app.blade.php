<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', '  ') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <link href="{{ asset('/css/layout.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="container-fluid ">
                <nav class="navbar navbar-default background-yellow  topbar" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                    </div>
                    <div class="collapse navbar-collapse navbar-fixed navbar-ex1-collapse">
                        <ul class="nav navbar-nav text-green bolder mayus arial principal-menu">
                          <li><a href="/">Home</a></li>
                          <li><a href="/">Privacy</a></li>
                          <li><a href="/register" @if (!Auth::guest()) style="display:none;" @endif>Sign Up</a></li>
                          <li><a href="/">FAQ</a></li>
                          <li><a href="/">Contact us</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>
                 <nav class="navbar navbar-default background-black topbar" role="navigation">
                    <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="" href="{{ url('/') }}">
                                   <img class="img-responsive" src="{{ asset('/img/logo1.png') }}">
                                </a>
                            </div>
                            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                                <ul class="nav navbar-nav">&nbsp;</ul>
                                <ul class="nav navbar-nav navbar-right text-white">
                                    <!-- Authentication Links -->
                                    @if (Auth::guest())
                                        <form id="signin" class="navbar-form navbar-right" role="form" method="POST" _lpchecked="1" action="{{ url('/login') }}">
                                            {{ csrf_field() }}
                                            <div class="input-group principal-login-input">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user text-white"></i></span>
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address" autocomplete="off">                                        
                                            </div>      
                                            <div class="input-group principal-login-input">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock text-white"></i></span>
                                                <input id="password" type="password" class="form-control" name="password" value="" placeholder="Password">                           @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif              
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-grey mayus text-black bolder arial without-border principal-login-btn">Login</button>
                                        </form>
                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                                        </span>
                                        @endif
                                    @else
                                    <li><a><b>My Balace</b> {{ Auth::user()->balance }} $</a></li>
                                    <li><a><b>My Invest</b> {{ App\Investment::where('user_id',Auth::user()->id)->where('status',1)->sum('investment') }} Usd</a></li>
                                    <li><a href="{{ url('/deposit') }}">Deposit</a></li>
                                    <li><a href="{{ url('/investment') }}">Investment</a></li>
                                    <li><a href="{{ url('/withdrawal') }}">Withdrawals</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{ url('/logout') }}"
                                                        onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                    </div>
                <!-- </div> /.navbar-collapse -->
                </nav>

        </div>
        @yield('content')
        
        <div class="container-fluid background-footer bolder">
            <div class="container background-footer bolder padding-top-15px">
                <div class="row">   
                <div class="col-xs-4"> 
                  <a class="" href="{{ url('/') }}"><img class="img-responsive img-center" src="{{ asset('/img/logofooter239x41.png') }}"></a>
                </div>
                <div class="col-xs-8"> 
                  <a class="" href="{{ url('/') }}"><img class="img-responsive img-center" src="{{ asset('/img/logoservers671x54.png') }}"></a>
                </div>
                </div>
            </div>
        </div>

        <div class="container-fluid background-green bolder">
            <div class="container background-green bolder padding-top-15px text-center">
                <div class="row text-center">   <a class="" href="{{ url('/') }}"><img class="img-responsive img-center" src="{{ asset('/img/GIF.gif') }}"></a> 
                </div>
                <div class="row text-black">  <h3>  Powered by <a href="#" class="owhyip text-white">OwHyip</a> </h3>
                </div>
            </div>
        </div>
    </div> {{-- app --}}
    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
