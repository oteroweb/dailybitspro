@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Investment</div>
                <div class="panel-body">
                @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                {!! Form::open(['route  ' => 'InverstMake']) !!}
                        {!! Form::token() !!}
                        <div class="form-group">
                            {!! Form::number('inverst', null, ['step'=>'0.01','class' => 'form-control', 'placeholder'=>'Enter your amount']) !!}
                        </div>              
                        {!! Form::submit('Inverst', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
