

$(window).scroll(function() {
  if ($(document).scrollTop() > 52) {
    $('#main-nav').addClass('fixed');
    $('.block').addClass('show');
    $('.main-menu-dropdown').fadeIn(400);
  } else {
    $('#main-nav').removeClass('fixed');
    $('.block').removeClass('show');
    $('.main-menu-dropdown').fadeOut(200);
  }
});