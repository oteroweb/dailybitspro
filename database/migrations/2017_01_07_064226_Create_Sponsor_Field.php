<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         

          if (!Schema::hasColumn('users', 'sponsor')) {
                Schema::table('users', function (Blueprint $table) {
            $table->string('sponsor',50)->nullable();
            $table->foreign('sponsor')->references('username')->on('users');

            }); }    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if (Schema::hasColumn('users', 'sponsor')) {
       Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['sponsor']);
            $table->dropColumn('sponsor');

        });
        }
    }
}
