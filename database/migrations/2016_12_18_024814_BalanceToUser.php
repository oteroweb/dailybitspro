<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BalanceToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasColumn('users', 'balance')) {
                Schema::table('users', function (Blueprint $table) {
                $table->float('balance', 10 ,6 )->default(0);
                $table->float('repurchase', 10 ,6 )->default(0);
            }); }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'balance')) {
       Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('balance');
            $table->dropColumn('repurchase');
        });
        }
    }
}
