<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('investments')) {
            Schema::create('investments', function (Blueprint $table) {
                $table->increments('id');
                $table->float('investment', 10, 6)->default(0);
                $table->float('final_investment', 10, 6)->default(0);
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->integer('created_att');
                $table->integer('finish_att');
                $table->integer('time_elapsed');
                $table->integer('status');
                $table->integer('time_left');
                $table->float('earnins_interval', 8, 6);    
                $table->integer('interval');
                $table->integer('loop');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if (Schema::hasTable('investment')) {

            Schema::drop('investment');
        }
    }
}
