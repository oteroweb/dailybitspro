<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWalletToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasColumn('users', 'perfectmoney')) {
                Schema::table('users', function (Blueprint $table) {
                $table->string('perfectmoney', 10 )->default(0);
            }); }
        if (!Schema::hasColumn('users', 'btcwallet')) {
                Schema::table('users', function (Blueprint $table) {
                $table->string('btcwallet', 50 )->default(0);
            }); }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'perfectmoney')) {
       Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('perfectmoney');
        });
        }

                if (Schema::hasColumn('users', 'btcwallet')) {
       Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('btcwallet');
        });
        }
    }
}
