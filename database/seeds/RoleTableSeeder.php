<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('roles')->delete();

      $role = [
				[
		        	'id' => 1,
	    	    	'name' => 'ohyip',
					'display_name' => 'Owner´s Script ',
					'description' => 'Owner´s Script or WebMaster',	
	        	],
	        	[
		        	'id' => 2,
	    	    	'name' => 'owner',
					'display_name' => 'Owner',
					'description' => 'Owner/Client Site ',	
	        	],
	        	[
		        	'id' => 3,
	    	    	'name' => 'admin',
					'display_name' => 'Admin Site',
					'description' => 'Admin/ Principal Manager Site ',	
	        	],
	        	[
		        	'id' => 4,
	    	    	'name' => 'manager',
					'display_name' => 'manager Site',
					'description' => 'Admin/Manager Site ',	
	        	],
	        	[
		        	'id' => 5,
	    	    	'name' => 'promoter',
					'display_name' => 'promoter Site',
					'description' => 'promoter Site ',	
	        	],
	        	[
		        	'id' => 6,
	    	    	'name' => 'client',
					'display_name' => 'User',
					'description' => 'Regular User Of Site ',	
	        	],
        ];
foreach ($role as $key => $value) {
        	App\Role::create($value);
        }
    }
}
