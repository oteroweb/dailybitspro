<?php

namespace App\Http\Controllers;
use Validator;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\investment;
use Illuminate\Support\Facades\Redirect;

use DB;

class InvestmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('investment.index');
    }

    public function makeInvest(Request $request)
    { 


// pendiente 
//       1.-) Gana el 3% diario de lunes a viernes
// 2.-) Inversión mínima:  7$
// 3.-)Retiro desde:  1$
// 4.-) Pagos manuales
// 5.-) Solicitudes de pago 1 por día
// 6.-) Procesador BITCOIN. Payeer y Perfectmoney
// 7.-) 3% por referido
// 8.-) 6% por representante
    	$inverst = $request->all(); 
    	$v = Validator::make($inverst, [ 'inverst' => 'checkfunds|required|numeric'], [ 'checkfunds' => 'Insufficient funds']
    		);
        if ($v->fails()) { return redirect('investment')->withErrors($v)->withInput();}
 		$id = intval(Auth::user()->id);
		$user = User::find($id);
		$myinverst= doubleval($inverst['inverst']);
		$mybalance= doubleval(Auth::user()->balance);
		$myinverstbalance= doubleval(Auth::user()->repurchase);

    	DB::transaction(function ($id) use($id, &$myinverst) {
    	$now = date_create();
    	$last = date_create('+24 hours');
    	$lastt = date_timestamp_get($last);
        $nowd = date_timestamp_get($now);
        $diference = $lastt - $nowd;  
    	 $status = 1;
        $earning = 1.2;
        $interval = 3600;
        $ciclos = $diference/$interval;
        $earnins_interval = ($myinverst*$earning)/$ciclos;
    		DB::table('users')->where('id', '=', $id)->decrement('balance', $myinverst);
		    DB::insert('insert into investments (`investment`, `user_id`,`final_investment`,`created_att`,`finish_att`,`time_elapsed`,`earnins_interval`,`interval`,`loop`,`time_left`,`status`) values (?, ?,?,?,?,?,?,?,?,?,?)', [$myinverst, $id, $myinverst*$earning,$nowd,$lastt,$nowd,$earnins_interval,$interval,$ciclos,0,$status]);

		});
    		$message =  ' '.Auth::user()->name.'&nbsp;investment made successfully for '.$myinverst.' $';
  //   		// $message =  html_entity_decode(	"&lt;strong&gt;Findx&lt;/strong&gt;");
    	return Redirect::back()->with('status',$message);
    	// return $message;

    }


public function getDateLastInvest()
{
  $invest = investment::where('status', 1)->where('user_id', Auth::user()->id)
               ->orderBy('created_at')
               ->take(1)
               ->first();
               if ($invest==null) {
                 return false;
               }
               else {
return $invest->time_elapsed;
               }
}

public function getIntervalLastInvest($id)
{
return 3600;
}
public function CheckOneHourAgo()
{
// return 1474005206
$now = date_timestamp_get(date_create('now'));
  $LastInvest = $this->getDateLastInvest();
  $interval = $this->getIntervalLastInvest(Auth::user()->id);
  $count = $now - $LastInvest;
  if ($LastInvest == false) { return false;  }
  elseif ($count < $interval) {return false;}
  else {return true;}

}
public function getEarnings()
  {
  $allMyInvest = investment::where('status', 1)->where('user_id', Auth::user()->id)
               ->orderBy('created_at')
               // ->take(1)
               // ->first();
               ->get();
               // ->count();
  foreach ($allMyInvest as $invest => $value) {
 //tomo el tiempo que ha pasado
    $id = $value->id;
    if ($value->loop <= 0) {DB::table('investment')->where('id', $id )->update(['status' => 0]);}
    if ($value->time_elapsed ==  $value->finish_att ) {DB::table('investment')->where('id', $id )->update(['status' => 0]);}
    

    $tiempopasado = $value->time_elapsed;
    $tiempofinal = $value->finish_att;


  //tomo tiempo actual
    $tiempoactual = date_timestamp_get(date_create('now'));
    $resta = $tiempoactual - $tiempopasado;
    $intervalo = $value->interval;
    $ciclos = intval($resta / $intervalo);
    if ($tiempoactual > $tiempofinal ) {
      $ciclos = $value->loop;
    }
    $nuevotiempoactual = ($ciclos*$intervalo) + $tiempopasado;  
    // echo $ciclos;
    $ganancias= $value->earnins_interval * $ciclos;
      $iduser=Auth::user()->id;
    echo $ciclos;

      DB::transaction(function ($id) use($id, &$ganancias, &$iduser,&$ciclos,&$nuevotiempoactual) {
      DB::table('users')->where('id', '=', $iduser)->increment('balance', $ganancias);
      DB::table('investment')->where('id', '=', $id)->decrement('loop', $ciclos);
    DB::table('investment')->where('id', $id )->update(['time_elapsed' => $nuevotiempoactual]);
    // echo $earnins_interval;
    //determinar cuanto tiempo ha pasado
    //convertir ese tiempo en tT    
  //tomar el tiempo en el que se creo el paquete
  // $creadot = $value->time_elapsed;
  //tomamos el tiempo que sobro en anteriores
  // $sobrat = $value->time_left;
  //loop
  // $loop = $value->loop;
  // le añadimos el tiempo que sobro del anterior transaccion a la actual 
  // $time_elapsed = $time_elapsed+$sobrat;
  //restamos y tomamos el tiempo que paso
  // $resta = (($time_elapsed) - ($creadot) );
  //se toma el intervalo
  // $intervalo = $this->getIntervalLastInvest($value->id);
  //se divide para obtener la diferencia
  // $diferencia = $resta / $intervalo; 
  // aqui se añade el valor 
    // agregar ganancias
    //maxreturn tiene la cifra dividido por 100
    // echo $id;
    // echo $creadot;
    // echo "<br>";
    // echo $creadot;
      //   $now = date_create();
      //   $last = date_create('+24 hours');
      //   $lastt = date_timestamp_get($last);
      //   $nowd = date_timestamp_get($now);
      //   $status = 1;
      //     $earning = 1.5;
      // DB::insert('insert into investment (`investment`, `user_id`,`final_investment`,`created_att`,`finish_att`,`time_elapsed`,`status`) values (?, ?,?,?,?,?,?)', [$myinverst, $id, $myinverst*$earning,$nowd,$lastt,$nowd,$status]);
      // DB::insert('insert into investment (`investment`, `user_id`, `created_at`, `finish_at`) values (?, ?)', [$myinverst, $id]);

      });

  //aqui se sabe cuanto se va a restar 
  // $resto =(intval($diferencia)*$intervalo);
  //nuevo sobra
  // $new_left =(($time_elapsed) - ($creadot+$resto) );
}
$message =  ' '.Auth::user()->name.'&nbsp; earnigs generate';
return Redirect::back()->with('status',$message);

}
}
