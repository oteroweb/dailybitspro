<?php

namespace App\Http\Controllers;
use Validator;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\investment;
use Illuminate\Support\Facades\Redirect;
use charlesassets\LaravelPerfectMoney\PerfectMoney;
Use oteroweb\LaravelCoinPayment\CoinPayment;

use DB;

class DepositController extends Controller
{
    public function index()
    {
        return view('deposit.index');
    }

    public function makeDeposit(Request $request)
    { 
        $deposit = $request->all(); 
        $v = Validator::make($deposit, [ 'method' => 'required','deposit' => 'min:7|required|numeric'], [ 'checkfunds' => 'Insufficient funds','method.required' => 'Select a method to payment']
            );
      
        if ($v->fails()) { return back()->withErrors($v)->withInput();}
    	if($_POST['method'] == 'pm') { 
    		$form = PerfectMoney::render(		['PAYEE_NAME' => Auth::user()->name,'PAYMENT_AMOUNT' => $_POST['deposit']]);
    		echo "<div style='display:none;'>".$form."<div>";
    		echo "<div>Please Wait a Second<div>";
			echo '<script type="text/javascript"> window.onload=function(){
          document.forms[0].submit(); } </script>'; }
		elseif (	$_POST['method'] == 'bit') { 
        $cp = New CoinPayment();
        // $cp = New CoinPayment;
        $cp->generateTransactionWithRedirect($request);
        }
    }

}
