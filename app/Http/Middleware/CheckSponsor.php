<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
class CheckSponsor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sponsor = $request->sponsor;
        $existe = User::where('username', $sponsor)->first();
        var_dump($existe);
        if (is_null($existe)) {
            return redirect('register');
        } else {
        return $next($request);

        }
    }
}
