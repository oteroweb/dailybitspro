<?php

namespace App\Providers;
use Validator;
use Auth;
use Illuminate\Support\ServiceProvider;

class CheckFundsProviders extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    Validator::extend('checkfunds', function ($attribute, $value, $parameters) {
    $myfunds = doubleval(Auth::user()->balance);

    $is_accepted = $myfunds >= $value;
    return $is_accepted;
});
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
