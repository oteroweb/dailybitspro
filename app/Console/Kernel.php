<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Auth;

use App\User;

use App\investment;

use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
          $schedule->call(function () {

           $inverstment = DB::table('investments')
                     ->select(DB::raw('sum(investment) as total'),'user_id')
                     ->where('status', 1)
                     ->groupBy('user_id')
                     ->get()
                     ;
           foreach ($inverstment as $key => $value) {
             DB::table('users')
               ->where('id', $value->user_id)
               ->update([
                   'balance' => DB::raw('balance + '.$value->total.' * 0.00125'),
               ]);
           }
        })->hourly()->weekdays();
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
